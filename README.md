# Dominic's README

Hello there :wave: Were you looking for something? I'm not sure what information could be relevant here so DM me in Slack or [open an issue](https://gitlab.com/dcouture/dcouture/-/issues) in my README project to let me know what would have been helpful to you. :)